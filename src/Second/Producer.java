package Second;
import java.util.Date;

public class Producer implements Runnable {
	private Queue queue;
	private final int TIMES = 100;

	public Producer(Queue queue) {
		this.queue = queue;
	}

	@Override
	public void run() {
		try {
			for (int i =0; i< TIMES;i++){
				String date = new Date().toString();
				queue.enqueue(date);
			}
		} catch (InterruptedException e) {
			e.printStackTrace();
		}

	}

}
