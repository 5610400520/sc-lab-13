package Second;
import java.util.ArrayList;
import java.util.concurrent.locks.Condition;
import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReentrantLock;

public class Queue extends ArrayList<String> {
	private Lock lock;
	private Condition con;
	
	public Queue(){
		lock = new ReentrantLock();
		con = lock.newCondition();
	}
	public void enqueue(String str) throws InterruptedException{
		lock.lock();
		try{
			while (this.size() >= 10){
				con.await();
			}
			add(str);
			con.signalAll();
		}finally{
			lock.unlock();
		}
		
	}
	public String dequeue() throws InterruptedException{
		String out;
		lock.lock();
		try{
			while (this.isEmpty()){
				con.await();
			}
			out = remove(0);
			con.signalAll();
		}finally{
			lock.unlock();
		}
		return out;
	}

}
