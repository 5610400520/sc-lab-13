package Second;
public class ProducerConsumerMain {

	public static void main(String[] args) {
		Queue queue = new Queue();
		Producer p = new Producer(queue);
		Consumer c = new Consumer(queue);
		Thread producer = new Thread(p);
		Thread consumer = new Thread(c);
		producer.start();
		consumer.start();	
	}
}