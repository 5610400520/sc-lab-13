package First;

import java.util.List;

public class DoProduction implements Runnable {
	   private List<String> listQueue;
	   public DoProduction(List<String> q) { 
		   listQueue = q; 
	   }
	 
	   public void run() {
	      try {
	         String result = listQueue.remove(0);
	         while (!result.equals("*")) {
	            System.out.println(Thread.currentThread().getName()
	               +": " + result );   
	            result = listQueue.remove(0);
	        }
	      }
	      catch (Exception e) {
	         System.out.println(Thread.currentThread().getName() 
	             + " " + e.getMessage());
	      }
	   }
	}