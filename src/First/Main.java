package First;

import java.util.Collections;
import java.util.LinkedList;
import java.util.List;

public class Main {
	public static void main(String[] args) throws Exception {
		List<String> list = Collections.synchronizedList(new LinkedList<String>());
		PrepareProduction prepare = new PrepareProduction(list);
		DoProduction doProduction1 = new DoProduction(list);
		DoProduction doProduction2 = new DoProduction(list);
		DoProduction doProduction3 = new DoProduction(list);
		DoProduction doProduction4 = new DoProduction(list);
		DoProduction doProduction5 = new DoProduction(list);
		Thread thread = new Thread(prepare);
		Thread thread1 = new Thread(doProduction1);
		Thread thread2 = new Thread(doProduction2);
		Thread thread3 = new Thread(doProduction3);
		Thread thread4 = new Thread(doProduction4);
		Thread thread5 = new Thread(doProduction5);
		thread.start();
		thread1.start();
		thread2.start();
		thread3.start();
		thread4.start();
		thread5.start();
		thread1.join();
		thread2.join();
		thread3.join();
		thread4.join();
		thread5.join();
		System.out.println("Done");
	}
}
